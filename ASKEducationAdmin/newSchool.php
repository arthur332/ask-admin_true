<?php
if($_REQUEST){
	include_once($_SERVER['DOCUMENT_ROOT'].'/connection.php');
	include_once($_SERVER['DOCUMENT_ROOT'].'/fileResize.php');
	
	define('ADODB_FETCH_DEFAULT',0);
	define('ADODB_FETCH_NUM',1);
	define('ADODB_FETCH_ASSOC',2);
	define('ADODB_FETCH_BOTH',3);
	
	
	function getFields($obj) {
		$temp  = array();
		$i = 0;
		while (!$obj->EOF) {
			$temp[$i] = $obj->fields;
			$obj->MoveNext();
			$i ++;
		}
		
		return $temp;
    }

	$data = $_REQUEST;
	
	$rs = $DB->Execute("INSERT INTO `schools` (`label`,`costCurrency`, `name`,`country`,`description_short`, `language`, `description_long`, `cost`, `costCalc`, `city`, `type`, `schoolBirth`, `schoolStartStudy`, `studentAge`, `studentAgeMiddle`, `studentsByClassRoom`, `popular`, `meta_keywords`, `meta_description`, `title`, `visible`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
	array($data["label"],
	$data["costCurrency"],
	$data["name"],
	$data["country"],
	$data["description_short"],
	$data["language"],
	$data["description_long"],
	$data["cost"],
	$data["cost"],
	$data["city"],
	$data["type"],
	$data["schoolBirth"],
	$data["schoolStartStudy"],
	$data["studentAge"],
	$data["studentAgeMiddle"],
	$data["studentsByClassRoom"],
	$data["popular"] == "on" ? "1":"0",
	$data["meta_keywords"],
	$data["meta_description"],
	$data["title"],
	$data["visible"] == "on" ? "1":"0"
	));
	
	if ($rs === false) {die("failed");}
	
	$fotoId = mysql_insert_id();
	
	/* local
	$galeryDir = "I:/projects/ASKEducation/ASKEducation/Content/images/school/galery/".$fotoId."/";
	$galeryDirBig = "I:/projects/ASKEducation/ASKEducation/Content/images/school/galery/".$fotoId."/big/";*/
	
	/* production*/
	$galeryDir = "../Content/images/school/galery/".$fotoId."/";
	$galeryDirBig = "../Content/images/school/galery/".$fotoId."/big/";
	
	
	if (!is_dir($galeryDir)) {
		mkdir($galeryDir); //create the directory
		chmod($galeryDir, 0777); //make it writable
	}
	
	if (!is_dir($galeryDirBig)) {
		mkdir($galeryDirBig); //create the directory
		chmod($galeryDirBig, 0777); //make it writable
	}
	
	
	$_F =& $_FILES['image'];
		
	if(!empty($_F['error'])){
		 switch($_F['error']){
		  case '1':
			$error = 'Загружаемый файл весит больше чем позволяют настройки сервера (upload_max_filesize directive in php.ini)';
		   break;
		  case '2':
			$error = 'Загружаемый файл весит больше чем позволяют настройки сервера (MAX_FILE_SIZE)';
		   break;
		  case '3':
			$error = 'Загруженный файл был залит частично';
		   break;
		  case '4':
			$error = false;//'Файл не выбран.';
		   break;
		  case '6':
			$error = 'не найдена временная директория';
		   break;
		  case '7':
			$error = 'Не удалось записать файл на диск';
		   break;
		  case '8':
			$error = 'File upload stopped by extension';
		   break;
		  case '999':
		  default:
			$error = 'No error code avaiable';
		 }
		 print_r($error);
	} elseif(empty($_F['tmp_name']) || ($_F['tmp_name'] == 'none')) {
		$error = 'No file was uploaded..';
	} else {
		/*local
		img_resize($_F['tmp_name'], "I:\projects\ASKEducation\ASKEducation\Content\images\school\/".$fotoId.".jpg", $width=210, $height=135, $quality=100);
		img_resize($_F['tmp_name'], "I:\projects\ASKEducation\ASKEducation\Content\images\school\big\/l_".$fotoId.".jpg", $width=300, $height=0, $quality=100);
		img_resize($_F['tmp_name'], "I:\projects\ASKEducation\ASKEducation\Content\images\school\big\/".$fotoId.".jpg", $width=600, $height=0, $quality=100);*/
		
		/*production*/
		img_resize($_F['tmp_name'], "..\Content\images\school\/".$fotoId.".jpg", $width=210, $height=135, $quality=100);
		img_resize($_F['tmp_name'], "..\Content\images\school\big\/l_".$fotoId.".jpg", $width=300, $height=0, $quality=100);
		img_resize($_F['tmp_name'], "..\Content\images\school\big\/".$fotoId.".jpg", $width=600, $height=0, $quality=100);
		$error = false;
	}
	
	if(!!$rs && !$error){
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/School');
	}else{echo $error;}
	
}

?>