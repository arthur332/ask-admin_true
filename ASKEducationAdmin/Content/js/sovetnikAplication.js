﻿var Sovetnik = function() {
    var self = this;

    /**
    * @name {_interface}
    * @param {Object} setup
    * @param {Object} extra <as like beforeSend:function(){}..>
    * @return {Object}
    * @use /-self._interface({type:'get'|'post', url:url|+,(data:{})|undefined}, false|true)-/
    */
    this.ajaxInterface = function (setup, extra) {
        var _self = this;
        /**@type {Object} base*/
        this.base = {
            url: null,
            type: 'post',
            data: null
        };

        /**@set {url} */
        this.base.url = setup.url;

        !!setup.type && setup.type.toLowerCase() === "get"
            ? delete this.base.type
            : this.base.data = setup.data;

        !!extra
            ? $.extend(_self.base, extra)
            : void (0);

        return $.ajax(_self.base);
    };

   /**
    * @name {dispatchEvent}
    * @param {Event} eventName
    * @param {Object|int|String|array|undefined} data
    * @return {Object} self
    */
    this.dispatchEvent = function (eventName, data) {
        $(document).trigger(eventName, data);
        return this;
    };
};
/**@init*/
var sovetnik = new Sovetnik();

/**@{grid system}*/
(function() {
    var b = $("div.s-grid");
    $(document).bind("keydown", function(a) {
        a = a || window.event;
        1 == a.altKey && (1 == a.shiftKey && 1 == a.ctrlKey) && (b.toggle(), b.height($(document).height() - b.offset().top));
    });
})();