﻿/**
 * @name {FilterController}
 * @author {Enokyan A/R}
 * @return {Object}
 */
var EditController = function () {
    var self = this;

    this.dataGot = 0;

    this.dataNeed = 1;

    this.getData = function (method, type) {
        self.method = method;

        askApp.ajaxInterface({ url: "/" + method.concat(".php"), data: { get: "list", type: type } }, false).done(self.getDataCallback);
        if (method === "school") {
            self.getSpecs();
        }
        if (method === "course" || method === "review") {
            self.dataNeed = 2;
            askApp.ajaxInterface({ url: "/school.php", data: { get: "list", type: "all" } }, false).done(self.getSchoolsCallback);
        }
    };

    this.getBindedSchools = function (id) {
        askApp.ajaxInterface({ url: self.method.concat(".php"), data: { get: "schools", id: id } }, false).done(self.getBindedSchoolsCallback);
    };

    this.getBindedSchoolsCallback = function (response) {
        if (!!response){
            self.bindedSchools = JSON.parse(response);

            EM.bindedSchools([]);

            for (var i in self.bindedSchools) {
                if (self.bindedSchools.hasOwnProperty(i)) {
                    EM.bindedSchools.push(self.bindedSchools[i]);
                }
            }
        }

    };

    this.dropSchoolBind = function (courseId, schoolId) {
        askApp.ajaxInterface({ url: "/" + self.method.concat(".php"), type: "post", data: { get: "dropSchoolBind", courseId: courseId, schoolId: schoolId } }, false).done(self.dropSchoolBindCallback);
    };

    this.dropSchoolBindCallback = function(response) {
        console.log("dropSchoolBindCallback", response);
    };

    this.bindSpec = function (specId, schoolId, action) {
        askApp.ajaxInterface({ url: "/" + self.method.concat(".php"), type: "post", data: { get: "bindSpec", specId: specId, schoolId: schoolId, action: action } }, false).done(self.bindSpecCallback);
    };

    this.bindSpecCallback = function (response) {
        !!JSON.parse(response).EOF ? $.alert("Успех", "success") : $.alert("Не удалось, позвони Артуру 89060480468");
    };

    this.getBindedSpecs = function () {
        askApp.ajaxInterface({ url: "/" + self.method.concat(".php"), type: "post", data: { get: "bindedSpecs", schoolId: EM.selectedItem().id } }, false).done(self.getBindedSpecsCallback);
    };
    this.getBindedSpecsCallback = function (response) {
        console.log("getBindedSpecsCallback", response);
        EM.currentSpecs($.map(JSON.parse(response), function (link) { return link.specId; }));
    };

    this.dropItem = function (id) {
        askApp.ajaxInterface({ url: "/" + self.method.concat(".php"), data: { get: "remove", rowId: id } }, false).done(self.dropItemCallback);
    };

    this.dropItemCallback = function (response) {
        console.log(response);
        document.location.reload();
    }

    this.getDataCallback = function (response) {
        var wysContent = null, wysContent2 = null;

        self.data = JSON.parse(response);

        EM.items([]);

        for (var i in self.data) {
            if (self.data.hasOwnProperty(i)) {
                EM.items.push(self.data[i]);
            }
        }
        if (EM.items().length) {
            switch (self.method) {
                case "blog":
                    wysContent = "description_full";
                    break;
                case "school":
                    wysContent = "description_long";
                    wysContent2 = "description_short";
                    break;
                case "course":
                    wysContent = "description";
                    break;
                case "review":
                    wysContent = "description_full";
                    break;
                case "pages":
                    wysContent = "description";
                    break;
                case "countries":
                    wysContent = "seo";
                    break;
            }
            
            EM.wysiwygObservable(EM.items()[0][wysContent]);
            EM.wysiwygObservable2(EM.items()[0][wysContent2]);
            
        }
        
        self.dataGot ++;
        askApp.dispatchEvent("dataGot");
       
    };

    this.getSchoolsCallback = function (response) {
        self.schools = JSON.parse(response);

        for (var i in self.schools) {
            if (self.schools.hasOwnProperty(i)) {
                EM.schools.push(self.schools[i]);
            }
        }

        self.dataGot++;
        askApp.dispatchEvent("dataGot");
        
    };

    this.getSpecs = function () {
        askApp.ajaxInterface({ url: self.method.concat(".php"), data: { get: "specs" } }, false).done(self.getSpecsCallback);
    };

    this.getSpecsCallback = function (response) {
        self.specs = JSON.parse(response);
        EM.specs(self.specs);
    };


    this.ternar = function(obj) {
        return !!obj ? obj : "";
    };

    this.init = function () {
        self.dataGot = 0;
        self.getData();
    };
};

var EC = new EditController();

/**
 * @name {FilterModel}
 * @return {undefined}
 */
var EditModel = function () {
    var self = this;

    this.items = ko.observableArray([]);
    
    this.selectedItem = ko.observable();

    this.labelProvider = ko.observable();
    this.label = ko.observable();
    this.setLabel = function() {
        switch (self.labelProvider()) {
            case "643": {self.label("Р");break;}
            case "840": {self.label("USD");break;}
            case "978": {self.label("EUR");break;}
            case "826": {self.label("GBP");break;}
            case "124": {self.label("CAD");break;}
            case "036": {self.label("AUD");break;}
            case "756": {self.label("CHF");break;}
            case "554": { self.label("NZD"); break; }
            case "156": { self.label("CNY"); break; }
        }
    };
    
    this.editLabel = ko.observable();

    this.setLabelEdit = function () {
        console.log(self.selectedItem().costCurrency);
        switch (self.selectedItem().costCurrency) {
            case "643": { self.editLabel("Р"); break; }
            case "840": { self.editLabel("USD"); break; }
            case "978": { self.editLabel("EUR"); break; }
            case "826": { self.editLabel("GBP"); break; }
            case "124": { self.editLabel("CAD"); break; }
            case "036": { self.editLabel("AUD"); break; }
            case "756": { self.editLabel("CHF"); break; }
            case "554": { self.editLabel("NZD"); break; }
            case "156": { self.editLabel("CNY"); break; }
        }
        
    };

    this.setCodeValuForEdit = function(code) {
        return code;
    }

    this.wysiwygObservable = ko.observable("");

    this.wysiwygObservable2 = ko.observable("");

    this.updateWysiwygObservable = function (data, event) {
        switch (EC.method) {
            case "blog":
                self.wysiwygObservable(self.selectedItem().description_full);
                break;
            case "school":
                self.wysiwygObservable(self.selectedItem().description_long);
                self.wysiwygObservable2(self.selectedItem().description_short);
                break;
            case "course":
                self.wysiwygObservable(self.selectedItem().description);
                break;
            case "review":
                self.wysiwygObservable(self.selectedItem().description_full);
                break;
            case "pages":
                self.wysiwygObservable(self.selectedItem().description);
                break;
            case "countries":
                self.wysiwygObservable(self.selectedItem().seo);
                break;
        }
        EM.setLabelEdit();
    };

    this.specs = ko.observableArray();

    /**
     * @descr : специализации которые надо проставить
     */
    this.currentSpecs = ko.observableArray();

    this.bindSpec = function (data, event) {
        EC.bindSpec(this.id, self.selectedItem().id, $(event.target || event.srcElement).prop("checked")?1:0);
    };



    /**@only course usage*/
    this.schools = ko.observableArray([]);
    this.selectedSchool = ko.observable();
    this.bindList = ko.observableArray([]);
    this.bindListView = ko.observableArray([]);

    this.addToList = function () {
        if (!!self.selectedSchool()) {
            if (self.bindList().indexOf(self.selectedSchool().id) < 0) {
                self.bindList.push(self.selectedSchool().id);
                //self.bindListView.push(self.selectedSchool().name);
                self.bindListView.push(self.selectedSchool());
            }
            
            self.selectedSchool(null);
        }
    };

    /**@for editing form*/
    this.bindedSchools = ko.observableArray([]);

    this.selectedSchoolAddBinding = ko.observable();

    /**@name{bindListToSendToServer} @descr {Список для отправки на сервер для добавления зависимостей к имеющимся}*/
    this.bindListToSendToServer = ko.observableArray([]);

    this.extendBindings = function () {

        if (self.bindListToSendToServer().indexOf(self.selectedSchoolAddBinding().id) < 0 && _.where(self.bindedSchools(), { id: self.selectedSchoolAddBinding().id }).length === 0) {
            self.bindedSchools.push(self.selectedSchoolAddBinding());
            self.bindListToSendToServer.push(self.selectedSchoolAddBinding().id);
        } else {
            $.alert("Такая школа уже имеет привязку!");
        }

        self.selectedSchoolAddBinding(null);
    };

    this.getBindeds = function() {
        EC.getBindedSchools(self.selectedItem().id);
        EM.updateWysiwygObservable();
    };

    this.removeItem = function () {
        if (confirm("Вы действитеьно хотите удалить " + EC.method)) {
            EC.dropItem(self.selectedItem().id);
        }
    };

    this.removeBind = function () {
        var th = this;
        if (confirm("уверены ?")) {            
            self.bindedSchools.remove(this);

            if (!this.schoolId && !this.courseId) {
                self.bindListToSendToServer.remove(function (item) { return item === th.id.toString(); });
            } else {
                EC.dropSchoolBind(this.courseId, this.schoolId);
            }
            
        }
    };

    this.removeBindFromCreate = function () {
        var th = this;
        
        self.bindListView.remove(this);

        self.bindList.remove(function (item) { return item === th.id.toString(); });

        
    };

    /**@for editing form*/

    /**@only course usage*/
};

var EM = new EditModel();

$(document).on("dataGot", function() {
    if (EC.dataNeed === EC.dataGot) {
        ko.applyBindings(EM);
        if (EM.selectedItem()){
            EC.getBindedSchools(EM.selectedItem().id);
            EC.getBindedSpecs();
        }
        //EM.editLabel(EC.data[0].label);
        EM.setLabelEdit();
    }
});
