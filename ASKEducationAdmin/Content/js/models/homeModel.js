﻿/**
 * @name {Perekop}
 * @void
 */
var VPES = function () {

    var self = this;

    this.dataGot = 0;

    this.dataNeed = 1;


    this.selectors = {};

    
    this.pies = {
        addressPie: "#addressPie"
    };

    /**
     * @name {getData}
     * @param {Date} start
     * @param {Date} end
     * @void
     */
    this.getData = function (start, end) {
        askApp.ajaxInterface({ type: "post", url: "/course.php", data: { get: "list", type: "all"} }, false).done(self.getDataCallback);
    };

    /**
     * @name {getDataCallback}
     * @param {JSON} response
     * @void
     */
    this.getDataCallback = function (response) {
        var collection = [];

        self.data = _.groupBy(JSON.parse(response), "type");
        
        for (var i in self.data) {
            if (self.data.hasOwnProperty(i)) {
                collection.push([i, self.data[i].length]);
            }
        }

        self.pieData = collection;
        self.initPie(self.pies.addressPie, collection);
        
        self.dataGot++;
        askApp.dispatchEvent("vpesCallbacks");
    };

    this.initPie = function (pieSelector, pieData) {
        return $(pieSelector).highcharts({
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: 'Курсы по категориям'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 65,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Курсы',
                data: pieData
            }]
        });
    };


    this.init = function () {
        self.dataGot = 0;
        self.getData(self.initialDateStamp, self.initialDateStamp);
        
        return self;
    };
};

var vpes = new VPES();

vpes.init();

var VPESModel = function () {
    var self = this;

    this.records = ko.observableArray([]);
};

var vpesModel = new VPESModel();
ko.applyBindings(vpesModel);