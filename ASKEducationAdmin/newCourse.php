<?php
if($_REQUEST){
	include_once($_SERVER['DOCUMENT_ROOT'].'/connection.php');
	
	define('ADODB_FETCH_DEFAULT',0);
	define('ADODB_FETCH_NUM',1);
	define('ADODB_FETCH_ASSOC',2);
	define('ADODB_FETCH_BOTH',3);
	
	
	function getFields($obj) {
		$temp  = array();
		$i = 0;
		while (!$obj->EOF) {
			$temp[$i] = $obj->fields;
			$obj->MoveNext();
			$i ++;
		}
		
		return $temp;
    }

	$data = $_REQUEST;
	
	$rs = $DB->Execute("INSERT INTO `courses` (`label`,`costCurrency`, `name`,`country`,`description`, `language`, `type`, `cost`, `costCalc`, `weeks`, `best`, `age`, `studentsCount`, `startCourse`, `languageLevel`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", array($data["label"], $data["costCurrency"], $data["name"], $data["country"], $data["description"], $data["language"], $data["type"], $data["cost"], $data["cost"], $data["weeks"], $data["best"] == "on" ? "1":"0", $data["age"], $data["studentsCount"], $data["startCourse"], $data["languageLevel"]));
	
	$id = mysql_insert_id();
	
	if(strlen($data["bindings"]) > 0){
		$bindings = split(",", $data["bindings"]);
		
		foreach ($bindings as $i) {
			$links = $DB ->Execute("INSERT INTO `links` (`schoolid`,`courseid`) VALUES (?,?)", array($i, $id));
		}
	}
	
	if ($rs === false) {die("failed");}
	
	
	if(!!$rs){
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/Course');
	}
}

?>