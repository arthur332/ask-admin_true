<?php
if($_REQUEST){
	include_once($_SERVER['DOCUMENT_ROOT'].'/connection.php');
	include_once($_SERVER['DOCUMENT_ROOT'].'/fileResize.php');
	
	define('ADODB_FETCH_DEFAULT',0);
	define('ADODB_FETCH_NUM',1);
	define('ADODB_FETCH_ASSOC',2);
	define('ADODB_FETCH_BOTH',3);
	
	
	function getFields($obj) {
		$temp  = array();
		$i = 0;
		while (!$obj->EOF) {
			$temp[$i] = $obj->fields;
			$obj->MoveNext();
			$i ++;
		}
		
		return $temp;
    }

	$data = $_REQUEST;
	
	$rs = $DB->Execute("UPDATE `courses` SET `label`=?,`costCurrency`=?, `name`=?,`country`=?,`description`=?, `language`=?, `type`=?, `cost`=?, `costCalc`=?,`weeks`=?,`best`=?,`age`=?,`studentsCount`=?,`startCourse`=?,`languageLevel`=? WHERE id =?", array($data["edit-label"], $data["edit-costCurrency"], $data["edit-name"], $data["edit-country"], $data["edit-description"], $data["edit-language"], $data["edit-type"], $data["edit-cost"], $data["edit-cost"], $data["edit-weeks"], $data["edit-best"] == "on" ? "1":"0", $data["edit-age"], $data["edit-studentsCount"], $data["edit-startCourse"], $data["edit-languageLevel"], $data["edit-id"]));

	if(strlen($data["bindings"]) > 0){
		$bindings = split(",", $data["bindings"]);
		
		foreach ($bindings as $i) {
			$links = $DB ->Execute("INSERT INTO `links` (`schoolid`,`courseid`) VALUES (?,?)", array($i, $data["edit-id"]));
		}
	}
	
	if ($rs === false) {die("failed");}
	

	if(!!$rs){
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/Course');
	}
}

?>