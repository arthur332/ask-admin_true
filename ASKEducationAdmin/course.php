<?php
if($_REQUEST){
	include_once($_SERVER['DOCUMENT_ROOT'].'/connection.php');
	
	define('ADODB_FETCH_DEFAULT',0);
	define('ADODB_FETCH_NUM',1);
	define('ADODB_FETCH_ASSOC',2);
	define('ADODB_FETCH_BOTH',3);
	
	function getFields($obj) {
		$temp  = array();
		$i = 0;
		while (!$obj->EOF) {
			$temp[$i] = $obj->fields;
			$obj->MoveNext();
			$i ++;
		}
		
		return $temp;
    }

	switch($_SERVER['REQUEST_METHOD']) {
		case 'GET' : $data  = &$_GET; 
			break;
		case 'POST': $data  = &$_POST;
			break;
	}
	
	if($data["get"] == "list") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
		 
		 $data["type"] != "all"  
         ? $rs = $DB->Execute("SELECT * FROM `courses` WHERE type =? ORDER BY `name` ASC", $data["type"])
		 : $rs = $DB->Execute("SELECT * FROM `courses` ORDER BY `name` ASC");
		 
		 echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "sort") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
         $rs = $DB->Execute("SELECT * FROM `courses` WHERE type =? ORDER BY ".$data['sortBy']." ASC", $data["type"]);
		 echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "filter") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
		 $data["filterValue"] != "" 
         ? $rs = $DB->Execute("SELECT * FROM `courses` WHERE type =? AND ".$data['filterBy']." =? ORDER BY `name` ASC", array($data['type'], $data["filterValue"]))
		 : $rs = $DB->Execute("SELECT * FROM `courses` WHERE type =? ORDER BY `name` ASC", $data["type"]);
		 echo json_encode(getFields($rs));
	}
	
	if($data["get"] == "currencies") {
		$currencies = simplexml_load_file("http://www.cbr.ru/scripts/XML_daily.asp");
		$currenciesJson = json_encode($currencies);
		echo($currenciesJson);
	}
	
	if($data["get"] == "course") {
		$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
		$course = $DB->Execute("SELECT * FROM `courses` WHERE id =? ", $data["id"]);

		echo json_encode(getFields($course));
		
	}
	
	if($data["get"] == "remove") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
         
		 $rs = $DB->Execute("DELETE FROM `courses` WHERE id =?", $data["rowId"]);
		 $rsl = $DB->Execute("DELETE FROM `links` WHERE courseId =?", $data["rowId"]);
		 echo json_encode($rs);
	}
	
	if($data["get"] == "dropSchoolBind") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
         
		 $rs = $DB->Execute("DELETE FROM links WHERE courseId = ? AND schoolId = ?", array($data["courseId"], $data["schoolId"]));
		 echo json_encode($rs);
	}
	
	if($data["get"] == "schools") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
          
		 $rs = $DB->Execute("SELECT * FROM `links` LEFT JOIN `schools` ON (`links`.`schoolid`=`schools`.`id`) WHERE `courseid` =?", $data["id"]);
		 
		 echo json_encode(getFields($rs));
	}
	
}

?>