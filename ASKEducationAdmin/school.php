<?php
if($_REQUEST){
	include_once($_SERVER['DOCUMENT_ROOT'].'/connection.php');
	
	define('ADODB_FETCH_DEFAULT',0);
	define('ADODB_FETCH_NUM',1);
	define('ADODB_FETCH_ASSOC',2);
	define('ADODB_FETCH_BOTH',3);
	
	function DB($query) {
		$result = mysql_query($query);
		return $result;
    }
	
	function getFields($obj) {
		$temp  = array();
		$i = 0;
		while (!$obj->EOF) {
			$temp[$i] = $obj->fields;
			$obj->MoveNext();
			$i ++;
		}
		
		return $temp;
    }

	switch($_SERVER['REQUEST_METHOD']) {
		case 'GET' : $data  = &$_GET; 
			break;
		case 'POST': $data  = &$_POST;
			break;
	}
	
	if($data["get"] == "list") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
		
         $rs = $DB->Execute("SELECT * FROM `schools` ORDER BY `id` ASC");
		 		 
		 echo json_encode(getFields($rs));
		

	}

    if($data["get"] == "specs") {
        $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;

        $rs = $DB->Execute("SELECT * FROM `spec` ORDER BY `id` ASC");

        echo json_encode(getFields($rs));


    }
	
	if($data["get"] == "remove") {
		 $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
         
		 $rs = $DB->Execute("DELETE FROM `schools` WHERE id =?", $data["rowId"]);
		 $rsl = $DB->Execute("DELETE FROM `links` WHERE schoolId =?", $data["rowId"]);
		 echo json_encode($rs);
	}

    if($data["get"] == "bindSpec") {
        $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
        if($data["action"] == 1){
            $links = $DB ->Execute("INSERT INTO `linksspec` (`schoolId`,`specId`) VALUES (?,?)", array($data["schoolId"], $data["specId"]));
            echo json_encode($links);
        }else if($data["action"] == 0){
            $rsl = $DB->Execute("DELETE FROM `linksspec` WHERE schoolId =? AND specId=?", array($data["schoolId"],$data["specId"]));
            echo json_encode($rsl);
        }

    }

    if($data["get"] == "bindedSpecs") {
        $ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
        $rs = $DB->Execute("SELECT * FROM `linksspec` WHERE schoolId =? ORDER BY `id` ASC", $data["schoolId"]);

        echo json_encode(getFields($rs));
    }



	//echo $data["get"];
	//echo $data["type"];
	
}
?>