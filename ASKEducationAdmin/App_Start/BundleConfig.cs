﻿using System.Web;
using System.Web.Optimization;

namespace evrica
{
    public class BundleConfig
    {
        // Дополнительные сведения о Bundling см. по адресу http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
 

            bundles.Add(new ScriptBundle("~/bundles/layout").Include());


            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/css/common.css",
                "~/Content/css/style.css"
                ));

        }
    }
}